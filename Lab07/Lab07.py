# -*- coding: utf-8 -*-
'''
@file Lab07.py

@brief This module reads the position of a tap on the touch sensor
@details This is a class/driver for use in reading the X Y position on a resistive touch panel.
The class contains 4 methods. Measuring X,Y, and Z idependently as digital values, and returning 
a tuple containing [X,Y] coordinates. For Measuring Z a binary 0 or 1 is returned based on  whether
or not the touch panel detects contact. The threshold value used in the Measure Z method,
May be different for different touch panels.
Source Code: https://bitbucket.org/CraigKimball/me405_lab/src/master/Lab07/Lab07.py

@author Craig Kimball

'''

import pyb
import utime



class TouchyPanel:
    '''
    @brief Class/driver that reads and returns position of contact on a resistive touch sensor.
    @details This Driver takes in pin object definitions and sizes of the panel from the user and can perform 4 things.
    It can give just the digital value of X or y. It can also return a tuple of position values [x,y] in cm giving
    true position on the resitive touch panel depending on how the user dedinied their coordinate center.
    
    '''
    def __init__(self,xp,xm,yp,ym,l,w,center):
        '''
        @brief Init method for Touchy panel class, takes inputs and stores
        @details Method that takes in inputs from the user. The first 4 inputs are pin
        objects that will be assigned to the FPC output. The next 2 are length and width of the board in cm.
        the last entry is a list of the digital center position on the board given in [x,y]
        @param xp Pin object for the x positive channel from the FPC
        @param xm Pin object for the x negative channel from the FPC
        @param yp Pin object for the y positive channel from the FPC
        @param ym Pin object for the y negative channel from the FPC
        @param l length of the board in cm
        @param w width of the board in cm
        @param center List of center position digital values given as [x,y]
        
        '''
        self.xpPin = xp
        self.xmPin = xm
        self.ypPin = yp
        self.ymPin = ym
        
        self.length = l
        self.width = w
        self.zero = center
        
    def MeasureX(self):
        '''
        @brief Finds the X location of the touch on the touch sensor
        @detalis Returns a ADC value that represents the X position where contact was made
        on the touch sensor. A delay of 5us is implemented into the method to allow for the sensor
        value to settle. the center X value is subtractd to determine which side of the board the value came from
        
        '''
        #Defining Pin types for measuring X based on handout
        Xp = pyb.Pin(self.xpPin,pyb.Pin.OUT_PP)
        Xm = pyb.Pin(self.xmPin,pyb.Pin.OUT_PP)
        Yp = pyb.Pin(self.ypPin,pyb.Pin.IN)
        Ym = pyb.Pin(self.ymPin,pyb.Pin.IN)
        
        #Setting Pin States
        Xm.low()
        Xp.high()
        Ym = pyb.ADC(Ym)
        #Sleeping to allow settling time before reading a value
        utime.sleep_us(5)
        self.X_raw = Ym.read()
        self.X_Dposition = self.X_raw - self.zero[0]
        self.length_count = 3800-200
        self.lengthIN = self.length/self.length_count
        self.X_position = self.X_Dposition * self.lengthIN
        return(self.X_position)
    
        
    def MeasureY(self):
        '''
        @brief Finds the Y location of the touch on the touch sensor
        @detalis Returns a ADC value that represents the X position where contact was made
        on the touch sensor. A delay of 5us is implemented into the method to allow for the sensor
        value to settle. the center Y value is subtractd to determine which side of the board the value came from
        
        '''
        #Defining Pin types for measuring X based on handout
        Xp = pyb.Pin(self.xpPin,pyb.Pin.IN)
        Xm = pyb.Pin(self.xmPin,pyb.Pin.IN)
        Yp = pyb.Pin(self.ypPin,pyb.Pin.OUT_PP)
        Ym = pyb.Pin(self.ymPin,pyb.Pin.OUT_PP)
        
        #Setting Pin States
        Ym.low()
        Yp.high()
        Xm = pyb.ADC(Xm)
        #Sleeping to allow settling time before reading a value
        utime.sleep_us(5)
        self.Y_raw = Xm.read()
        self.Y_Dposition = self.Y_raw - self.zero[1]
        self.width_count = 3650-500
        self.WidthIN = self.width/self.width_count
        self.Y_position = self.Y_Dposition * self.WidthIN
        return(self.Y_position)
    
    
    def MeasureZ(self):
        '''
        @brief Finds the Z location of the touch on the touch sensor
        @detalis Returns a ADC value that represents when contact is made
        on the touch sensor. A delay of 5us is implemented into the method to allow for the sensor
        value to settle. the center Z value is subtractd to determine which side of the board the value came from
        
        '''
        Z_base = 3800
        #Defining Pin types for measuring X based on handout
        Xp = pyb.Pin(self.xpPin,pyb.Pin.IN)
        Xm = pyb.Pin(self.xmPin,pyb.Pin.OUT_PP)
        Yp = pyb.Pin(self.ypPin,pyb.Pin.OUT_PP)
        Ym = pyb.Pin(self.ymPin,pyb.Pin.IN)
        
        #Setting Pin States
        Xm.low()
        Yp.high()
        Ym = pyb.ADC(Ym)
        #Sleeping to allow settling time before reading a value
        utime.sleep_us(5)
        #Converting digital signal to a simple Binary output for checking if contact is made
        self.Z_raw = Ym.read()
        if self.Z_raw < Z_base:
            Z_position = 1
        else:
            Z_position = 0
        
        return(Z_position)

    def ReadPosition(self):
        '''
        @brief Returns an X Y position on the board if it detects Z has been pressed
        @details
        '''
        
        self.length_count = 3800-200
        self.width_count = 3650-500
        #converting from digital signal to a dimension of inches
        self.lengthIN = self.length/self.length_count
        self.WidthIN = self.width/self.width_count
        
        #Sampling Z first to see if the board is pressed then checking other values
        self.Z_value = self.MeasureZ()
        if self.Z_value == 1: 
            self.cordinate = [self.MeasureX(), self.MeasureY(),self.MeasureZ()]
            return(self.cordinate)
        else:
            return('No sensor contact detected')
        
        
        

