# -*- coding: utf-8 -*-
"""
Created on Sun Feb 28 07:50:49 2021

@author: craig
"""

import pyb
from Lab07 import TouchyPanel
import utime

l = 7.5
w = 4.5
center = [2000, 2000]

xp = pyb.Pin(pyb.Pin.board.PA7)
xm = pyb.Pin(pyb.Pin.board.PA1)
yp = pyb.Pin(pyb.Pin.board.PA6)
ym = pyb.Pin(pyb.Pin.board.PA0)

touch = TouchyPanel(xp,xm,yp,ym,l,w,center)

while True:
    position = touch.ReadPosition()
    print(position)