# -*- coding: utf-8 -*-
"""
@file Lab07_report
@page Lab07_sec  Lab07 Resistive Touch Panel Driver
@section Background_sec Background and objectives
The goal of this lab was to make a driver for measuring the position of contact on
a resitive touch panel. Resistive touch panels work by having vertical and horizontal lines
of resistors laying across each other on the surface. When one direction of resisors is
energized and contact is made on the panel the resistance at the contat point changes as the two
layers of resistors at that point are pushed together. A schematic of what this looks like is found below:
    
@image html Lab07_touch_schem.png

Depending on where contact is made on the board the voltage divider ratio between the two directional pads
changes which can be measured and converted to a distance value. When there is no contact the
node between the dividers is open which means one can also measure if contact is made with the board.

For this lab we used a setup picutred below

@image html Lab07_board.png

The board sends data through a flexible printed Circuit (FPC) to a breakout sensor that
reads the data and splits it into 4 channels xp, xm, yp, and ym respectively. This board is 
soldered to the base of the balancing platform to ground it and the FPC is plugged into the board.
A cable then plugs into the pins and down to the X6 pin connector on the board as shown.

@image html Lab07_plug.png

The orientation of this cable is important as we want it to reflect the given table in the Lab handout
so we know which pin objects will be used on the board for reading the values. 

@image htm Lab07.pins.png

The last column in this table shows us the CPU pins assigned to each male pin the connector
is attached to which lets us know what pins to set in our driver.

For the driver a set of methods were asked to be included and performance requirements were given
    - A constructor that allows the user to select four arbitrary pins representing xp, xm, yp, and ym
    as well as the width and length of the resistive touch panel and the coordinate representing
    the center of the resistive touch panel.
    
    -Three individual methods that scan the X, Y, and Z components respectively.
        -# The X and Y components should return values within the range of width and length
        specified in the constructor such that the readings are zero if the point of contact is in
        the middle of the touch panel
        
        -# The Z component should return as a Boolean value representing whether or not something is in contact with the touch panel.
        
    - A method that reads all three components and returns their values as a tuple.
    
    -You must be able to read from a single channel in less than 500µs and all three channels in
    less than 1500µs
    
To look at the functionality of the Driver please refer to Lab07.py in the File list

@section Testing_sec Testing

To ensure the functionality of the driver testing needed to be done to observe that:
    - each driver method worked appropriately and the returned value seemed accurate
    - the response time of the driver took less than 1500us when reading all positions
    
In order to measure this a test scripts was utililized. The script checked the response time of the driver TouchyPanel and returned a single
printed tuple of the detected position on the panel.

    import pyb
    from Lab07 import TouchyPanel
    import utime

    l = 7.5
    w = 4.5
    center = [2000, 2000]

    xp = pyb.Pin(pyb.Pin.board.PA7)
    xm = pyb.Pin(pyb.Pin.board.PA1)
    yp = pyb.Pin(pyb.Pin.board.PA6)
    ym = pyb.Pin(pyb.Pin.board.PA0)

    touch = TouchyPanel(xp,xm,yp,ym,l,w,center)

    start_time = utime.ticks_us()

    Value = touch.ReadPosition()
    end_time = utime.ticks_us()
    print(Value)
    total_time = utime.ticks_diff(end_time,start_time)
    print(total_time)

Here you can see how each input was definied for the constructor and the method use to record response time.
During the initial testing the driver responded at 1700us and by optimizing pin definitions within the driver
and retesting with this script the new repsonse time is 1580us which is close to the 1500us goal making this a valid driver.



@author: craig
"""

