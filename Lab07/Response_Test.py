# -*- coding: utf-8 -*-
"""
@file Response_Test.py

@brief This program is used to find the total time in micro seconds that it takes for
the touch panel Driver to record a position
@details This program defines Pins for the Touch Panel class and then has it take a single measurement after recording the system time.
When the methods is called it takes a certain amount of time to respond due to delay from redefining pin objects and the built in 
delay to x,y,z measuremnts. Once the measurement is returned the machine time is recorded and used to find time needed to take measurement.
The goal being to make a Driver that takes less than 1500us to respond.
@author: craig
"""

import pyb
from Lab07 import TouchyPanel
import utime

l = 7.5
w = 4.5
center = [2000, 2000]

xp = pyb.Pin(pyb.Pin.board.PA7)
xm = pyb.Pin(pyb.Pin.board.PA1)
yp = pyb.Pin(pyb.Pin.board.PA6)
ym = pyb.Pin(pyb.Pin.board.PA0)

touch = TouchyPanel(xp,xm,yp,ym,l,w,center)

start_time = utime.ticks_us()

Value = touch.ReadPosition()
end_time = utime.ticks_us()
print(Value)
total_time = utime.ticks_diff(end_time,start_time)
print(total_time)
    