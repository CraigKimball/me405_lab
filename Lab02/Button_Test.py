
"""
@File Lab01.py
@brief This module runs a reflex test for the user turning on an LED and measuting how long it takes the user to turn it off
@details This program randomly turns on an LED anytime after 2 seconds have elapsed and begins recording time until
the user presses the button to turn off the LED. The button acts as an interrupt to the code turning off the LED and resetting the timer.
The LED wil keep turning on until the user presses "Ctrl+c" at which point the Program will stop and the average reaction time will be 
given to the user across all button pushes trialed.

@author: craig
"""

import pyb
import utime
import random

# Setting up the LED to be an Output Pin
pinA5 = pyb.Pin(pyb.Pin.cpu.A5,pyb.Pin.OUT_PP)
# Setting up timer 2 wit a large period to count in micr0seconds
tim = pyb.Timer(2, prescaler= 79 , period = 0x7FFFFFFF)

#variables to store values from each run of the reflex test
time = 0
Button_Counter = 0
 
def LightOff (which_pin):        
    '''
    @brief This function is the intterupt service routine for when the button is pushed
    @details This service routine will intterupt our code whenever PC13 button is pushed. It will 
    turn off the LED and record how long the LED was on before the button was pressed. Code for the intterupt taken and modified
    from Lab handout documentation.
    
    @parameter which_pin Checks the first entry of the interrupt service routine to find which i/o pin will act as the interrupt.
    '''
    pinA5.low()
    global time
    time = time + tim.counter()
    
    global Button_Counter
    Button_Counter += 1
extint = pyb.ExtInt (pyb.Pin.board.PC13,   # Which pin
             pyb.ExtInt.IRQ_FALLING,       # Interrupt on falling edge
             pyb.Pin.PULL_UP,             # Activate pullup resistor
             LightOff)                   # Interrupt service routine


print('Starting Reflex Test')
try:    
    while True:   
        utime.sleep (2)
        
        LED = random.randrange(2)
        if LED == 1:
            tim.counter(0)
            pinA5.high()
            
except KeyboardInterrupt:
    average = (time /1000000) / Button_Counter
    print(Button_Counter)
    print("Test finished your average time was" + str(average))


        

