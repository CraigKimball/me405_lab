# -*- coding: utf-8 -*-
"""
@file Lab01.py

@brief This Finite state machine controls the operation of a Virtual Vending Machine

@details A virtual vending machine is being controlled by a finite state machine in this module.
A user can input coins or bills to the machine and then make a selection. If the amount is sufficent the machine
will dispense the virtual beverage and give change in coins and bills to the user. The user can also eject the money
they have put in the machine if they no longer want a virtual beverage.
Source Code: https://bitbucket.org/CraigKimball/me405_lab/src/master/Lab01/Lab01.py

@image html Lab01.png width=800cm
@author Craig Kimball
"""
import keyboard 

state = 0
pushed_key = None

def getPayment(payment):
    """
    @brief Displays Payment given 
    
    @details Sums up cuurency in tuple and spits out a decimal value for use in displaying currency.
    this can be used for counting money for a payment or change. The input must be a Tuple with 7 entries.
    The tuple order is as follows 0 = pennies, 1 = nickles, 2 = dimes, 3 = quarters, 4 = dollar, 5 = FiveDollar, 6 = TenDollar, 7 = TwentyDollar.
    """
    pennies      = int(payment[0])
    nickels      = int(payment[1])*5
    dimes        = int(payment[2])*10
    quarters     = int(payment[3])*25
    oneDollar    = int(payment[4])*100
    fiveDollar   = int(payment[5])*500
    tenDollar    = int(payment[6])*1000
    TwentyDollar = int(payment[7])*2000
    
    #This variable sums up the total amount of money given into an integer in terms of pennies.
    Total_given = (pennies + nickels + dimes + quarters + oneDollar + fiveDollar + tenDollar + TwentyDollar)/100
    return(Total_given)

def getChange(price,payment):
    """ 
    @brief Calculates the change to be given back depending on the price and the amount given as payment
    
    @details This function takes a price input as a decimal value and a tuple with 7 entries representing the amount of money given. The 
    tuple is broken up into types of currecny based on indices as follows: 0 = pennies, 1 = nickles, 2 = dimes, 3 = quarters, 4 = dollar, 5 = FiveDollar, 6 = TenDollar, 7 = TwentyDollar.
    The function will return a Tuple with exact change prioritzing the least amount of coins and bills possible.
    """
     # defining variables for each space as a form of currency
    pennies      = int(payment[0])
    nickels      = int(payment[1])*5
    dimes        = int(payment[2])*10
    quarters     = int(payment[3])*25
    oneDollar    = int(payment[4])*100
    fiveDollar   = int(payment[5])*500
    tenDollar    = int(payment[6])*1000
    TwentyDollar = int(payment[7])*2000
    
    #This variable sums up the total amount of money given into an integer in terms of pennies.
    Total_given = (pennies + nickels + dimes + quarters + oneDollar + fiveDollar + tenDollar + TwentyDollar)
    
    Total_change = Total_given - (price*100)
    
    
    # Zeroing all the change trackers
    TwentyChange  = 0
    TenChange     = 0
    FiveChange    = 0
    OneChange     = 0
    QuarterChange = 0    
    DimeChange    = 0
    NickelChange  = 0
    PennyChange   = 0
    
    # Checking to see if user put in correct amount of money.
    if Total_change < 0:
        return(None)
    
    #conditionals for dividing the change into units of currency prioritizing lowest amount of currency
    if(Total_change >= 2000):
         Total_change_new = Total_change % 2000
         TwentyChange = (Total_change - Total_change_new) /2000
         Total_change = Total_change_new
         
    if(Total_change >= 1000):
         Total_change_new = Total_change % 1000
         TenChange = (Total_change - Total_change_new) /1000
         Total_change = Total_change_new
    if(Total_change >= 500):
         Total_change_new = Total_change % 500
         FiveChange = (Total_change - Total_change_new) /500
         Total_change = Total_change_new
         
    if(Total_change >= 100):
         Total_change_new = Total_change % 100
         OneChange = (Total_change - Total_change_new) /100
         Total_change = Total_change_new
         
    if(Total_change >= 25):
         Total_change_new = Total_change % 25
         QuarterChange = (Total_change - Total_change_new) /25
         Total_change = Total_change_new
         
    if(Total_change >= 10):
         Total_change_new = Total_change % 10
         DimeChange = (Total_change - Total_change_new) /10
         Total_change = Total_change_new
         
    if(Total_change >= 5):
         Total_change_new = Total_change % 5
         NickelChange = (Total_change - Total_change_new) /5
         Total_change = Total_change_new
         
    if(Total_change >= 1):
         Total_change_new = Total_change % 1
         PennyChange = (Total_change - Total_change_new) /1
         Total_change = Total_change_new
        
    ChangeGiven = [PennyChange, NickelChange, DimeChange, QuarterChange, OneChange, FiveChange, TenChange, TwentyChange]
    
    return(ChangeGiven)
    pass

def printWelcome():
    """
    @brief Prints a VendotronˆTMˆ welcome message with beverage prices
    
    """
    print('Welcome to VendoTron ^TM^ please make a selection. For Cuke(1.25) press C. For Popsi(1.25) press P. For Spryte(1.00) Press S. For Dr.Pupper(1.50) Press D.')
    pass



def on_keypress (thing):
    """ 
    @brief Callback which runs when the user presses a key.
    
    """
    global pushed_key

    pushed_key = thing.name

keyboard.on_press(on_keypress)
try:
    while True:
    
        """
        Implement FSM using a while loop and an if statement
        will run eternally until user presses CTRL-C
        """
    
        if state == 0:
            printWelcome()
            ## Price of Cuke
            Cuke = 1.25
            ## Price of Popsi
            Popsi = 1.25
            ## Price of Spryte
            Spryte = 1.00
            ## Price of Dr. Popper
            Dr_Popper = 2.00
            ##Empty payment typle
            # Tuple representing US currency. indices 0 = pennies, 1 = nickel, 2 = dimes, 3 = quarters, 4 = one dollar, 5 = five dollar, 6 = ten dollar, 7 = twenty dollar.
            Payment = [0, 0, 0, 0, 0, 0, 0, 0]
            Payment_display = 0
            round_table_count = 0
            state =1
            
        # Waiting state where currency is added, rejected, ejected, or beverage is selected
        elif state == 1:
                if pushed_key:
                    if pushed_key == "0":
                        Payment[0] += 1
                        Payment_display = getPayment(Payment)
                        print ('Penny added =' + str(Payment_display))
                        pushed_key = None
                        state == 1
                        
                    if pushed_key == "1":
                        Payment[1] += 1
                        Payment_display = getPayment(Payment)
                        print ('Nickel added Total balance =' + str(Payment_display))
                        pushed_key = None
                        state == 1
                        
                    if pushed_key == "2":
                        Payment[2] += 1
                        Payment_display = getPayment(Payment)
                        print ('Dime added detected Total balance =' + str(Payment_display))
                        pushed_key = None
                        state == 1
                        
                    if pushed_key == "3":
                        Payment[3] += 1
                        Payment_display = getPayment(Payment)
                        print ('Quarter added Total balance =' + str(Payment_display))
                        pushed_key = None
                        state == 1
            
                    if pushed_key == "4":
                        Payment[4] += 1
                        Payment_display = getPayment(Payment)
                        print ('One Dollar added Total balance =' + str(Payment_display))
                        pushed_key = None
                        state == 1
                        
                    if pushed_key == "5":
                        Payment[5] += 1
                        Payment_display = getPayment(Payment)
                        print ('Five Dollar added Total balance =' + str(Payment_display))
                        pushed_key = None
                        state == 1
            
                    if pushed_key == "6":
                        Payment[6] += 1
                        Payment_display = getPayment(Payment)
                        print ('Ten Dollar added Total balance =' + str(Payment_display))
                        pushed_key = None
                        state == 1
        
                    if pushed_key == "7":
                        Payment[7] += 1
                        Payment_display = getPayment(Payment)
                        print ('Twenty Dollar added Total balance =' + str(Payment_display))
                        pushed_key = None
                        state == 1
                        
                    if pushed_key == "c":
                        print('checking amount of money')
                        if Payment_display >= Cuke:
                            print('dispensing Cuke')
                            Change = getChange(Cuke, Payment)
                            Change_displayed = getPayment(Change)
                            print('your Change is ' + str(Change_displayed))
                            Change = 0
                            Change_displayed = 0
                            Payment = [0, 0, 0, 0, 0, 0, 0, 0]
                            state == 1
                            pushed_key = None
                        else: 
                            print('insufficent funds please add more money')
                            round_table_count += 1
                            pushed_key = None
                            state == 1
                            
                    if pushed_key == "p":
                        print('checking amount of money')
                        if Payment_display >= Popsi:
                            print('dispensing Popsi')
                            Change = getChange(Popsi, Payment)
                            Change_displayed = getPayment(Change)
                            print('your Change is' + str(Change_displayed))
                            Change = 0
                            Change_displayed = 0
                            Payment = [0, 0, 0, 0, 0, 0, 0, 0]
                            state == 1
                            pushed_key = None
                        else: 
                            print('insufficent funds please add more money')
                            pushed_key = None
                            state == 1
                        
                    if pushed_key == "s":
                        print('checking amount of money')
                        if Payment_display >= Spryte:
                            print('dispensing Spryte')
                            Change = getChange(Cuke, Payment)
                            Change_displayed = getPayment(Change)
                            print('your Change is' + str(Change_displayed))
                            Change = 0
                            Change_displayed = 0
                            Payment = [0, 0, 0, 0, 0, 0, 0, 0]
                            state == 1
                            pushed_key = None
                        else: 
                            print('insufficent funds please add more money')
                            pushed_key = None
                            state == 1
                        
                    if pushed_key == "d":
                        print('checking amount of money')
                        if Payment_display >= Dr_Popper:
                            print('dispensing Dr.Popper')
                            Change = getChange(Cuke, Payment)
                            Change_displayed = getPayment(Change)
                            print('your Change is' + str(Change_displayed))
                            Change = 0
                            Change_displayed = 0
                            Payment = [0, 0, 0, 0, 0, 0, 0, 0]
                            state == 1
                            pushed_key = None
                        else: 
                            print('insufficent funds please add more money')
                            pushed_key = None
                            state == 1
        
                    if pushed_key == "n" and round_table_count > 5: 
                        print ("Vendotron Demands a Shrubbery mortal")
                        selection = 4
                        pushed_key = None
                        state == 1            
        
                    
             
                

except KeyboardInterrupt:
    Payment_display = getPayment(Payment)
    print ("Control-C has been pressed: dispensing money you get:" + str(Payment_display))
    keyboard.unhook_all ()
