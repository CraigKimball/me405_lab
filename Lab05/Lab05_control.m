syms b theta theta_dot theta_ddot x x_dot x_ddot rb rc rg rm mb mp g Tx Ib Ip lp lr rp

M = [ -(mb*(rb^2) + mb*rb*rc + Ib)/rb ,  -(Ib*rb + Ip*rb + mb*(rb^3) + mb*rb*(rc^2) + 2*mb*(rb^2)*rc + mp*rb*(rg^2) + mb*(rb^2))/rb;
      -(mb*(rb^2) + Ib)/rb            ,  -(mb*(rb^3) + mb*rc*(rb^2)+Ib*rb)/rb  ];
  
u = [theta_ddot;
        x_ddot];
    
f = [b*theta_dot-g*mb*theta*(rb+rc)+x+((Tx*lp)/rm) + 2*mb*theta_dot*x*x_dot-g*mp*rg*theta;
     -mb*rb*x*(theta_dot^2)-g*mb*rb*theta];
 

 %Minv = inv(M);
 q_ddot = M\f

 ddtx = [ x_dot;
          theta_dot;
          q_ddot(1,1);
          q_ddot(2,1)];
          
      
 Jx = jacobian(ddtx, [x_dot; theta_dot; x; theta]);
 Ju = jacobian(ddtx, [Tx]) ;
 

 
 A_var = subs(Jx,{x_dot,theta_dot,x,theta}, {0,0,0,0});
 B_var = subs(Ju,{Tx},{0});
 
 A_sym = subs(A_var,{rm,lr,rb,rg,lp,rp,rc,mb,mp,Ip,b,g,Ib}, {60,50,10.5,42,110,32.5,50,30,400,1.88e6,10000,9810,1323});
 B_sym = subs(B_var,{rm,lr,rb,rg,lp,rp,rc,mb,mp,Ip,b,g,Ib}, {60,50,10.5,42,110,32.5,50,30,400,1.88e6,10000,9810,1323});
 
 A = double(A_sym)
 B = double(B_sym)
 
 %% Open Loop Model

 