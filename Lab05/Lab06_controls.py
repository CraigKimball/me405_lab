# -*- coding: utf-8 -*-
"""
@file Lab06_controls.py

@page Lab06_sec  Lab06 System Modeling
@section Introduction_sec Linearization of model
This Lab was done with Miles Young another student in the lab. His documentation can also be found at
https://milesyoung.bitbucket.io/. For this lab we had to build on the work done in Lab 05 and 
create a system model for the equations created from Lab05. From Lab05 we ended up with two second order ODE
In order to create a system model these needed to be first order linear equations. The first step then was to linearize the
equations from Lab05. Following the state space linearization method shown in Piazza the team used matlab to generate the
Jacobians used to find the 1st order equaitons. Once found we plugged in the given values from Lab06 converting all distance to units in meters [M]

@image html Lab06_lin1.png
@image html Lab06_lin2.png
@image html Lab06_lin3.png

With these two Matrixes we are then able to use a state space variable system to model are platform.


@section Sim_sec Simulation Model
The team used Simulink to model there bal platform assembly. Two models were created one open loop and one closed loop. They are pictured below.

@image html Lab06_model.png

In the first Open system model we used to step functions to model an impulse torque. The steps are set to step up and down respectively at the same
time stamp and the magnitude is based on a variable set in the Matlab program. In both examples we used a State-Space block to calculate the outputs and then a demux
to split them into 4 seperate variables that we could call for plotting. 

In the Closed Loop system a negative feedback loop feeds back into the summing junction just before the State Space equations. 
There is a gain value, K_gain, which is multiplied to the feedback signal before being sum which is defined by a variable K_gain in the workspace, based off
the values the Matrices gave us. 

For the initial conditions of each scenario variables in the workspace were used so that the team could use the same simulink models for each
scenario by redefining the initial conditions and run time after each run and subequent plotting. Below is an image of how we set these conditions

@image html Lab06_IC.png


@section OpenScenarios_sec Open Loop System Responses
For the open loop system the team was asked to run 4 scenarios to show the system modeling was done correctly. The
first scenario involved the ball sitting in the center of the platform. Both the ball and the platform were at rest and no torque was applied to the motor. The simulation was run
for 1 second.

@image html Lab06_op1.png

Looking above at the reposne from the system we can see that the ball or platform exhibits no change in position or velocity. This makes sense because
the ball started at the cg of the platform meaning the platform will not tilt as there is no moment caused from the ball resting away from the cg. Because there is
no torque applied the system stays at this equilibrim.

In the second scenario the platform still had a rotation and velocity of 0, but the ball was moved 5cm away from the centroid, with no torque applied from the system. The simulation was 
run for 0.4 seconds

@image html Lab06_op2.png

Unlike Scenario 1 the ball exerted a moment on the platform as the vector created from its weight was no longer inline with the Cg of the platform.
This causes the platform to begin tilting, gaining roatational position and velocity, which casues the ball to also begin moving. In this example the offset causes the ball to move in the
positive Y axis

In Scenario 3 The ball was left at the Cg, but the platform was titled 5 degrees or .0873 radians. Again the simulation was run for 0.4 seconds and no torque was applied to the motor.

@image html Lab06_op3.png

We see that the response to these initial conditions is very similar to the conditions from Scenario 2. Because the platformed started ina tilted position,
the Normal force acting on the ball decreased causing the force of friciton to decrease, and thus the ball started rolling. As the ball rolled farther away from the Cg
this also casued the platform to continue tilting.

In Scenario 4 the ball at at rest above the Cg of the platform. The platform had no rotation but, an inpule Torque of 1 mNm*s was applied to the motor. The simulation was run for 0,4 seconds

@image html Lab06_op4.png

In this scenario the impulse Torque at the beginning of the recording causes the system to fall out of equilibrium. The platform immediately
gains an angular veocity and begins rotating away from 0 radians. As this happens the ball like in Scenario three begins moving with the platform, but gains velcity at a slower
rate than the platform as friction tries to preven the ball from moving. This can be observed when comparing the two velcoity plots above.


@section ClosedScenarios_sec Closed Loop System Response

For This final response we used the K_gain values given i the LAb handout and seen in the modeling section to create a negative feedback loop.
The feedback loop used the form u = -Kx, where u is the summed input into the State space, K is the given gain matrix, and x is the output. This
simulation was run for 20 seconds in order to observe any converging or diverging trends.

@image html Lab06_cs1.png

From the plots above it is clear that the system is overdamped. The position and velocities of both the ball and the platform converge
on zero as the system attempts to bring both the ball and platform back to 0 rotation with the ball resting above the platform cg. This scenario
verifies that our values and closed loop model are working properly as this is the desired repsonse of the physical system.

@author Craig Kimball, Miles Young
"""

