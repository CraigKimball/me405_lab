# -*- coding: utf-8 -*-
"""
@file_Lab05_hand_page.py

@page Lab05  Lab05 Kinematics
@section Assumption_sec Introduction and Assumptions
This Lab was done with Miles Young another student in the lab. His documentation can also be found at
https://milesyoung.bitbucket.io/. For this lab we had to develop equations for use in the final lab project 
of creating a ball balancing platform. Specifically for this lab we developed equations to relate the linear accelaration
of the ball and Angular accelaration of the platform to torque supplied by motors connected to pushrods on the edge of the balancing platform.
A simplified picture of the platform is given below
@image html Lab05_layout.png width=800cm

With this simplified side view given to us the following  assumptions were given in the lab handout for us to use.

1. To greatly simplify the analysis, consider two planes of motion for the platform and assume
that the two are uncoupled. That is, instead of modeling the system as a balancing platform
that can move in two degrees of freedom, you will model the system as a pair of balancing
beams that each move in one degree of freedom.

2. You may use the small angle approximation wherever appropriate. Near the equilibrium
orientation, the geometry of the platform, lever arms, and push-rods should keep everything
very close to perpendicular, making this assumption valid.

3. The platform has by far the most significant mass and moment of inertia for the system.
You may therefore assume that the masses and inertias of the lever arm and push-rod are
negligible.

4. In order for the ball to be controllable, it must remain in contact with the plate and roll without slipping. In practice, this will not always be the case, but assuming a no-slip condition
will be necessary to perform this analysis.

5. Assumee that the force acting on the platform at point Q is purely vertical due to the small angle
approximation. This means that the X and Y componets of the reaction force at point Q are zero.

Below is the analysis we did to find the set of equations desired.

@section Eom_sec Equations of Motion
First Equations of Motion needed to be established for the system. Specificlally we needed to relate
the torque of the Motor at point A to the rotation of the Platform at point O. In the work below Torque
was written as Phi to represent angular rotatio about the x axis and Theta y represents platform rotation about the Y axis.

@image html Lab05_EOM.png

Notice above that we included the linear accelaration of the ball as a function of the platform roatating. This will be important for a later step of the analysis.


@section Kinematics_sec Kinematics

The team decoupled the system into three seperate Free Body Diagram, Mass Accelaration Diagram pairings. One for the Push Rod Linkage, one for
the Platform, and a final one for just the ball. Using the small Angle approximation and assuming Rqx and rqy were zero from earlier assumptions the team
devleoped equations putting angular accelaration of the platform and Linear accelarartion of the ball in terms of the other forces in the system. This 
was done by taking the sum of moments about different points in the FBD's as instructed in the lab handout

@image html Lab05_kin1.png
@image html Lab05_kin2.png
@image html Lab05_kin3.png

Looking at the last equaiton for the ball you can see that we substituted the Angular accelaration term of the ball
for the EOM equation found earlier to relate the Linear accelaration of the pall to angle of the platform.

@section Matrix_sec Analytical Model
For the final step of this lab we were asked to develop an analytical model for the lab using the equations
and two remaining unknowns found from the kinematics work done above. The analytical model created is given below.

@image html lab05_analytic.png

"""

