
"""
@File Homeowrk0x01.py
@author: craig
@brief Change machine

"""



def getChange(price, payment):
    
    # defining variables for each space as a form of currency
    pennies      = int(payment[0])
    nickels      = int(payment[1])*5
    dimes        = int(payment[2])*10
    quarters     = int(payment[3])*25
    oneDollar    = int(payment[4])*100
    fiveDollar   = int(payment[5])*500
    tenDollar    = int(payment[6])*1000
    TwentyDollar = int(payment[7])*2000
    
    #This variable sums up the total amount of money given into an integer in terms of pennies.
    Total_given = (pennies + nickels + dimes + quarters + oneDollar + fiveDollar + tenDollar + TwentyDollar)
    
    Total_change = Total_given - (price*100)
    
    
    # Zeroing all the change trackers
    TwentyChange  = 0
    TenChange     = 0
    FiveChange    = 0
    OneChange     = 0
    QuarterChange = 0    
    DimeChange    = 0
    NickelChange  = 0
    PennyChange   = 0
    
    # Checking to see if user put in correct amount of money.
    if Total_change < 0:
        return(None)
    
    #conditionals for dividing the change into units of currency prioritizing lowest amount of currency
    if(Total_change >= 2000):
         Total_change_new = Total_change % 2000
         TwentyChange = (Total_change - Total_change_new) /2000
         Total_change = Total_change_new
         
    if(Total_change >= 1000):
         Total_change_new = Total_change % 1000
         TenChange = (Total_change - Total_change_new) /1000
         Total_change = Total_change_new
    if(Total_change >= 500):
         Total_change_new = Total_change % 500
         FiveChange = (Total_change - Total_change_new) /500
         Total_change = Total_change_new
         
    if(Total_change >= 100):
         Total_change_new = Total_change % 100
         OneChange = (Total_change - Total_change_new) /100
         Total_change = Total_change_new
         
    if(Total_change >= 25):
         Total_change_new = Total_change % 25
         QuarterChange = (Total_change - Total_change_new) /25
         Total_change = Total_change_new
         
    if(Total_change >= 10):
         Total_change_new = Total_change % 10
         DimeChange = (Total_change - Total_change_new) /10
         Total_change = Total_change_new
         
    if(Total_change >= 5):
         Total_change_new = Total_change % 5
         NickelChange = (Total_change - Total_change_new) /5
         Total_change = Total_change_new
         
    if(Total_change >= 1):
         Total_change_new = Total_change % 1
         PennyChange = (Total_change - Total_change_new) /1
         Total_change = Total_change_new
        
    ChangeGiven = [PennyChange, NickelChange, DimeChange, QuarterChange, OneChange, FiveChange, TenChange, TwentyChange]
    
    return(ChangeGiven)


if __name__ == "__main__":
    payment = [3, 0, 0, 2, 1, 0, 0, 1]
    price = 17.50
    pass