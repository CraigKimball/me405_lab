# -*- coding: utf-8 -*-
"""
Created on Thu Jan 28 10:31:18 2021

@author: craig
"""
import pyb
import array

tim2 = pyb.Timer(2, prescaler= 79 , period = 100)

buffy = array.array ('H', (0 for index in range (100)))
time  = array.array ('H', (0 for index in range (100)))
PinA0 = pyb.Pin(pyb.Pin.board.PA0)
adc = pyb.ADC(PinA0)
adc.read_timed(buffy,tim2)
print(buffy)
