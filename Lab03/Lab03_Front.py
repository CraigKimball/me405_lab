
"""
@file Lab03_Front.py
@brief This module is the user interface for interacting with the nucleo, and generates response plots of the voltage measured on the Nucleo
@details This module sets up the serial connection with the nucleo and then waits for the user to ask to begin
data colleciton. Once the user enters "g" the module then waits until it receives data back from the nucleo over the serial port.
Once received the data is broken up into a Voltage and Time array and is then plotted. Below is a result plot from a run of this code.
Source Code: https://bitbucket.org/CraigKimball/me405_lab/src/master/Lab03/Lab03_Front.py
@image html Lab03_Plot.png width=500cm
@author: craig
"""
import serial
import numpy as np
import matplotlib.pyplot as plt
import time

#opening serial connection
ser = serial.Serial(port='COM3',baudrate=115273,timeout=1)

# Setting up the lists used for plotting data
Voltage = []
Time    = []
converted = None

inv = input('press g then enter to begin data colletion.')
ser.write(str(inv).encode('ascii'))
print('input received begin data collection')

# While loop that puts the program in waititng until it sees information waiting to be read in serial buffer
while True:
    
    if ser.in_waiting != 0:
        break


while(converted != 'END'):
    raw_data = ser.readline().decode('ascii')
 #Takes raw data and removes the /n from the string
    converted = raw_data.strip()
 #creates a temporary list that holds the time and Encoder pos value split from string into a list
    temp_list = converted.split(',')
    
 # appends temporary list values to local lists as floating point 
    if(temp_list[0] != 'END'):
        print(temp_list)
        Time.append(float(temp_list[0]))
        Voltage.append(float(temp_list[1]))
    
    else: 
        ser.close()
        break
data1 = np.asarray(Time,Voltage)

#saving array as a CSV file
np.savetxt('Digital_signal_v_time',data1,delimiter=",")


plt.plot(Time,Voltage, "--b")
plt.xlabel('Time (us)')
plt.ylabel('digital signal')

plt.show()
