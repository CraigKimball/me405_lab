# -*- coding: utf-8 -*-
"""
@file Lab03_Back.py
@brief Reads the voltage value across the button ,PC13, and when it detects the button is pushed records voltage data and sends to computer
@details This module sits in waiting until it receives the letter g in ASCII from the front end over serial port. Once
received the module then begins sampling data in 100 us looking to catch the button being pressed. Once the user has pressed the button
and the module caught the sampling of the voltage rising back up, it turns an LED off to signal data was collected and then begins sending the recorded
data over serial port back to the Front end. For this experiment to work the button pin needs to be connected to Pin A0-A5. This module was used 
with Pin A0, a photo o the board setup is supplied below. To run this program on startup of the Nucleo save and load it onto the microboard as 'main.py'
Source Code: https://bitbucket.org/CraigKimball/me405_lab/src/master/Lab03/Lab03_Back.py

@image html Lab03_board.png width=800cm

@author: craig
"""
import pyb
from pyb import UART
import array
import utime

# Timer 2 ticking at 1 microsecond
Timer_p = int(100)
pinA5 = pyb.Pin(pyb.Pin.cpu.A5,pyb.Pin.OUT_PP)
#Setting up timer and UART connection
tim2 = pyb.Timer(2, prescaler= 79 , period = Timer_p)
uart = UART(2)
#Setting up arrays to hold recorded values for data, becasue we cannot record
# time and voltage using adc class time array is generated based on sample time
buffy = array.array ('H', (0 for index in range (100)))
time  = array.array ('H', ( index + 1 for index in range (100)))

#Setting up pins and ADC reader
PinA0 = pyb.Pin(pyb.Pin.board.PA0)
adc = pyb.ADC(PinA0)
U_input = 0

#Looking for G to be pressed 
while True:
    if uart.any() != 0:
        U_input = uart.readchar()
    # If G is pressed
    if U_input == 103:
        pinA5.high()
        tim2.counter(0)
        adc.read_timed(buffy,tim2)
    if buffy[0] < 10 and buffy[-1] > 3000 :
        pinA5.low()
        break 
    
for n in range (len(time)):
    print('{:},{:}'.format(time[n],buffy[n]))
print('END')
   
        



        
        
